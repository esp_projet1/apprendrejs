var seconds = 0;
var minutes = 0;
var hours = 0;
function chrono () {
    if (seconds == 60) {
        seconds = 0;
        minutes++;
    }
    if (minutes == 60) {
        minutes = 0;
        hours++;
    }
    h = hours;
    m = minutes;
    s = seconds;
    if (s < 10) s = "0" + s;
    if (m < 10) m = "0" + m;
    if (h < 10) h = "0" + h;
    document.form1.chrono.value = h + ":" + m + ":" + s;
    seconds++;
}